# TinyMCE as a GraphCMS UI extension

⚠️ UI Extensions are still in early alpha and need to be activated on your GraphCMS project beforehand

This is a simple demo showcasing how to use a self-hosted TinyMCE as a UI Extension in GraphCMS.
